package model

type localStatus map[int64]int64

func (l localStatus) isMatch(statusId, ownerId int64) bool {
	if v, ok := l[ownerId]; ok {
		if v == statusId {
			return true
		}
	}
	return false
}

func (l localStatus) isHave(ownerId int64) bool {
	_, ok := l[ownerId]
	return ok
}

func (l localStatus) add(statusId, ownerId int64) {
	ok := l.isMatch(statusId, ownerId)
	if ok {
		return
	}
	l[ownerId] = statusId
}

func (l localStatus) del(ownerId int64) {
	if l.isHave(ownerId) {
		delete(l, ownerId)
	}
	return
}

func (l localStatus) update(ownerId, statusId int64) bool {
	if !l.isHave(ownerId) {
		return false
	}

	l.del(ownerId)
	l.add(statusId, ownerId)
	return true
}

var localstatus localStatus
