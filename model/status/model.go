package status

const (
	apiSatatus     string = "https://api.renren.com/v2/status"
	apiSatstusList string = "/list"
)

type status struct {
	Id             int64
	OwnerId        int64
	Content        string
	CreateTime     string
	ShareCount     int
	CommentCount   int
	SharedStatusId int64
	SharedUserId   int64
}

type response struct {
	Response status
}

type responses struct {
	Response []status
}
