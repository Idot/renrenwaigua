package comment

const (
	apiComment    string = "https://api.renren.com/v2/comment"
	apiCommentPut string = "/put"
)

const (
	TYPE_SHARE  = "SHARE"
	TYPE_ALBUM  = "ALBUM"
	TYPE_BLOG   = "BLOG"
	TYPE_STATUS = "STATUS"
	TYPE_PHOTO  = "PHOTO"
)

//struct post to the server
type CommentPut struct {
	Content      string
	TargetUserId int64
	CommentType  string
	EntryOwnerId int64
	EntryId      int64
}

//struct for decode the response body
type commentResponse struct {
	Id           int64
	Content      string
	Time         string
	CommentType  string
	EntryId      int64
	EntryOwnerId int64
	AuthorId     int64
}

//struct for decode the response
type putResponse struct {
	Response commentResponse
}
