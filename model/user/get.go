package user

import (
	"encoding/json"
	"fmt"
	"net/url"
	"renrenwaigua/oauth"
	"strconv"
)

const (
	apiUrl          string = "https://api.renren.com/v2"
	apiUserLoginGet string = "/user/login/get"
	apiUserGet      string = "/user/get"
)

const (
	TYPE_HEAD  string = "HEAD"
	TYPE_TINY  string = "TINY"
	TYPE_MAIN  string = "MAIN"
	TYPE_LARGE string = "LARGE"
)

type image struct {
	//the size of image,like large,head and so on
	Size string
	//the url of the image
	Url string
}

type sex string

//here must be (s sex),not (s *sex)
func (s sex) String() string {
	switch s {
	case "FEMALE":
		return "女性"
	case "MALE":
		return "男性"
	default:
		return string(s)
	}
}

type homeTown struct {
	Province string
	City     string
}

type basicInformation struct {
	//female and male
	Sex      sex
	Birthday string
	HomeTown homeTown
}

type educationBackGround string

func (e educationBackGround) String() string {
	switch e {
	case "DOCTOR":
		return "博士"
	case "COLLEGE":
		return "本科"
	case "GVV":
		return "校工"
	case "PRIMARY":
		return "小学"
	case "OTHER":
		return "其他"
	case "TEACHER":
		return "教师"
	case "MASTER":
		return "硕士"
	case "HIGHSCHOOL":
		return "高中"
	case "TECHNICAL":
		return "中转技校"
	case "JUNIOR":
		return "初中"
	case "SECRET":
		return "保密"
	default:
		return string(e)
	}
}

type school struct {
	Name string
	//the date you enter the school
	Year                string
	EducationBackGround educationBackGround
	Department          string
}

type industry struct {
	IndustryCategory string
	IndustryDetail   string
}

type job struct {
	JobCategory string
	JobDetail   string
}

type work struct {
	Name     string
	Time     string
	Industry industry
	Job      job
}

type likeCatagory string

func (l likeCatagory) String() string {
	switch l {
	case "SPORT":
		return "运动"
	case "MOVIE":
		return "电影"
	case "CARTOON":
		return "动漫"
	case "GAME":
		return "游戏"
	case "MUSIC":
		return "音乐"
	case "BOOK":
		return "书籍"
	case "INTEREST":
		return "爱好"
	default:
		return string(l)
	}
}

type like struct {
	Catagory likeCatagory
	Name     string
}

type emotionalState string

func (e emotionalState) String() string {
	switch e {
	case "INLOVE":
		return "恋爱中"
	case "OTHER":
		return "其他"
	case "SINGLE":
		return "单身"
	case "MARRIED":
		return "已婚"
	case "UNOBVIOUSLOVE":
		return "暗恋"
	case "DIVORCE":
		return "离异"
	case "ENGAGE":
		return "订婚"
	case "OUTLOVE":
		return "失恋"
	default:
		return string(e)
	}
}

type user struct {
	Id               int64
	Name             string
	Avatar           []image
	Star             int
	BasicInformation basicInformation
	Education        []school
	Work             []work
	Like             []like
	EmotionalState   emotionalState
}

type info struct {
	Id             int64
	Name           string
	Img            string
	Star           string
	Sex            string
	Birthday       string
	HomeTown       string
	Education      []*school
	Work           []*work
	Like           []*like
	EmotionalState string
}

type Json struct {
	Response user
}

func (j Json) GetInfo(TYPE string) (Info info) {
	Info.Id = j.Response.Id
	Info.Name = j.Response.Name
	Info.Img = ""
	for _, v := range j.Response.Avatar {
		if v.Size == TYPE {
			Info.Img = v.Url
			break
		}
	}
	if j.Response.Star == 1 {
		Info.Star = "星级用户"
	} else {
		Info.Star = "非星级用户"
	}
	Info.Sex = j.Response.BasicInformation.Sex.String()
	Info.Birthday = j.Response.BasicInformation.Birthday
	Info.HomeTown = j.Response.BasicInformation.HomeTown.Province + " " + j.Response.BasicInformation.HomeTown.City

	edu := make([]*school, len(j.Response.Education))
	for k, v := range j.Response.Education {
		var t school
		t = v
		edu[k] = &t
	}
	Info.Education = edu

	wor := make([]*work, len(j.Response.Work))
	for k, v := range j.Response.Work {
		var t work
		t = v
		wor[k] = &t
	}
	Info.Work = wor

	lik := make([]*like, len(j.Response.Like))
	for k, v := range j.Response.Like {
		var t like
		t = v
		lik[k] = &t
	}
	Info.Like = lik

	Info.EmotionalState = j.Response.EmotionalState.String()

	return Info
}

func LoginGet(t *oauth.Transport) (usr Json) {
	token := t.Token.AccessToken
	c := t.Client()

	vals := url.Values{}
	vals.Set("access_token", token)
	url := apiUrl + apiUserLoginGet + "?" + vals.Encode()

	r, err := c.Get(url)
	if err != nil {
		fmt.Println("apiuserget: get error:", err)
		return
	}
	defer r.Body.Close()
	if err = json.NewDecoder(r.Body).Decode(&usr); err != nil {
		fmt.Println(err.Error())
	}
	if r.StatusCode != 200 {
		fmt.Println("Login Get:", r.Status)
		fmt.Println("Login Get URL:", url)
	}
	return usr
}

func UserGet(t *oauth.Transport, id int64) (usr Json) {
	token := t.Token.AccessToken
	uid := strconv.FormatInt(id, 10)
	c := t.Client()

	vals := url.Values{}
	vals.Set("userId", uid)
	vals.Set("access_token", token)
	url := apiUrl + apiUserGet + "?" + vals.Encode()

	r, err := c.Get(url)
	if err != nil {
		fmt.Println("apiuserget: get error:", err)
		return
	}
	defer r.Body.Close()
	if err = json.NewDecoder(r.Body).Decode(&usr); err != nil {
		fmt.Println(err.Error())
	}
	if r.StatusCode != 200 {
		fmt.Println("User Get:", r.Status)
		fmt.Println("Login Get URL:", url)
	}
	return usr
}
